package pdu

import (
	"encoding/hex"
	"errors"
)

type PDU struct {
	SMSCLen   byte   // Length of SMSC information
	SMSSubmit byte   // First octet of SMS-SUBMIT message
	TPMsgRef  byte   // TP-Message-Reference (typically 0 to allow phone to set it)
	AddrLen   byte   // Length of phone number
	AddrType  byte   // Type of address (81 or 91, typically)
	PhoneNum  string // Phone Number
	TPPID     byte   // TP-PID protocol identifier (00)
	TPDCS     byte   // TP-DSC data coding scheme. (00 = 7-bit, 02 = 8-bit)
	TPVPF     byte   //  Validity period, may or may not be present
	DataLen   byte   // Length of user data, in septets if TPDCS = 00, in octets if TPDCS = 02
	Data      []byte // Raw user data
	Message   string // The converted message
}

/*
 * Given a string containing a hex number, unpack it as a SMS in PDU format.
 * Returns the unpacked structure
 * Example usage: PduUnpack("0001000b815190341141f0000002c834")
 */
func PduUnpack(s string) (p PDU, err error) {
	b, err := hex.DecodeString(s)
	if err != nil {
		return
	}

	if b[0] != 0 {
		errors.New("didn't expect SMSC length > 0, sorry")
		return
	}

	p.SMSCLen = b[0]
	p.SMSSubmit = b[1]
	p.TPMsgRef = b[2]
	p.AddrLen = b[3]
	p.AddrType = b[4]

	// We are given the length of the phone number,
	// but if it is odd it will have an "F" at the end for padding.
	readlen := p.AddrLen
	if readlen%2 == 1 {
		readlen++
	}

	num := s[10 : readlen+10] // phone number starts at offset 10
	p.PhoneNum = unswizzle([]byte(num), int(p.AddrLen))

	offset := 4 + (readlen / 2)

	p.TPPID = b[offset]
	p.TPDCS = b[offset+1]
	p.TPVPF = b[offset+2]
	p.DataLen = b[offset+3]

	p.Data = b[offset+4:]

	/* Unpack the packed septets. This is almost certainly a terrible
	 * way to do it, but I'm sick of thinking about the bastard
	 */
	bits := 0
	var result []byte
	idx := 0
	for i := 0; i < int(p.DataLen); i++ {
		var nb byte
		if bits != 7 {
			nb = p.Data[idx] & (0x7f >> byte(bits))

			if bits != 0 {
				nb = nb << byte(bits)
				nb |= p.Data[idx-1] >> byte(8-bits)
			}
		} else {
			nb = p.Data[idx] >> 1
		}
		result = append(result, nb)
		bits = (bits + 1) % 8

		if bits != 7 {
			idx++
		}
	}

	p.Message = string(result)

	return
}

func unswizzle(s []byte, l int) string {
	for i := 0; i < len(s); i += 2 {
		a := s[i]
		s[i] = s[i+1]
		s[i+1] = a
	}

	if len(s) > l {
		s = s[:l]
	}

	return string(s)
}
