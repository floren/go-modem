package pdu

import "testing"
import "fmt"

func TestPduUnpack(t *testing.T) {
	const sample = "0001000b815190341141f0000002c834"

	s, _ := PduUnpack(sample)
	fmt.Printf("%#v\n", s)
	s, _ = PduUnpack("0001000B811000000000F0000030C834888E2ECBCB2E978B8AAE8362323A1AA4ACB34132584C068AC57430980B1603C1DB20D4B1495DC552")
	fmt.Printf("%#v\n", s)
}
