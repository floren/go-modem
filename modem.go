package main

import (
	"bitbucket.org/floren/go-modem/pdu"
	"bufio"
	"fmt"
	"flag"
	"net"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type Status struct {
	regstatus   int  // first parameter, <mode>, of COPS
	regformat   int  // format of <oper> field in COPS
	pin         int  // PIN
	creg, cgreg int  // enable unsolicited registration messages
	mr          byte // message reference, a cyclic counter
}

var querypattern = ".*\\+.+\\?"
var assignpattern = ".*\\+.+\\=.*"
var OK = "\r\nOK\r\n"
var modemstatus Status
var expectedpin = 1234

var moutput *bufio.Writer // modem output socket
var minput *bufio.Reader  // input

var socketLoc = flag.String("socket", "/tmp/phone", "location to create communication socket")

func usage() {
	fmt.Fprintf(os.Stderr, "usage: go-modem\n")
	flag.PrintDefaults()
	os.Exit(2)
}

func cregmsg() {
	var creg int

	for {
		if creg != modemstatus.creg && modemstatus.creg == 2 {
			moutput.WriteString("\r\n+CREG: 1,\"036D\",\"58B2\"\r\n" + OK)
			fmt.Print("sent unsolicited CREG message\n")
			creg = modemstatus.creg
		}
		time.Sleep(time.Second * 10)
	}
}

func cgregmsg() {
	var cgreg int

	for {
		if cgreg != modemstatus.cgreg && modemstatus.cgreg == 1 {
			moutput.WriteString("\r\n+CGREG: 1\r\n" + OK)
			fmt.Print("sent unsolicited CGREG message\n")
			cgreg = modemstatus.cgreg
		}
		time.Sleep(time.Second * 10)
	}
}

func parseQuery(s string) (ret string, waserror bool) {
	// Default response
	ret = ""
	waserror = false

	// Chop off the "AT+", if it is there, or remove an initial "+"
	s = strings.Replace(s, "AT+", "", 1)
	if s[0] == '+' {
		s = s[1:]
	}

	switch s {
	case "CFUN?":
		ret = "\r\n+CFUN: 1\r\n"
	case "CPIN?":
		ret = "\r\n+CPIN: READY\r\n"
	case "CGREG?":
		ret = "\r\n+CGREG: " + strconv.Itoa(modemstatus.cgreg) + ",1\r\n"
	case "CREG?":
		ret = "\r\n+CREG: " + strconv.Itoa(modemstatus.creg) + ",1,\"036D\",\"58B2\"\r\n"
	case "COPS?":
		ret = fmt.Sprintf("\r\n+COPS: %d,%d,", modemstatus.regstatus, modemstatus.regformat)
		switch modemstatus.regformat {
		case 0:
			ret = ret + "\"Test Network\""
		case 1:
			ret = ret + "\"TEST\""
		case 2:
			ret = ret + "\"00101\""
		}
		ret = ret + "\r\n"
		fmt.Printf("> %s", ret)
	}
	return
}

var CRSMtab = map[string]string{
	"192,28589,0,0,15": "+CRSM: 144,0,000000046fad04000aa0aa01020000",
	"192,28618,0,0,15": "+CRSM: 144,0,0000000a6fca040011a0aa01020105",
	"192,28433,0,0,15": "+CRSM: 144,0,000000016f11040011a0aa01020000",
	//"192,28619,0,0,15": "ERROR: BAD COMMAND",
	"192,28435,0,0,15": "+CRSM: 144,0,000000016f13040011a0aa01020000",
	"192,28486,0,0,15": "+CRSM: 148,4",
	"192,28621,0,0,15": "+CRSM: 148,4",
	"192,28613,0,0,15": "+CRSM: 144,0,000000f06fc504000aa0aa01020118",
	"192,28472,0,0,15": "+CRSM: 144,0,0000000f6f3804001aa0aa01020000",
	"192,28438,0,0,15": "+CRSM: 144,0,000000026f1604001aa0aa01020000",
	//"192,28437,0,0,15": "ERROR: BAD COMMAND",
	"176,12258,0,0,10": "+CRSM: 144,0,98101430121181157002",
	"178,28480,1,4,32": "+CRSM: 144,0,ffffffffffffffffffffffffffffffffffff07815155255155f4ffffffffffff",
	"178,28617,1,4,4":  "+CRSM: 144,0,01000000",
	"176,28589,0,0,4":  "+CRSM: 144,0,00000003",
	"178,28618,1,4,5":  "+CRSM: 144,0,0000000000",
	"176,28433,0,0,1":  "+CRSM: 144,0,55",
	"176,28435,0,0,1":  "+CRSM: 144,0,55",
	"178,28613,1,4,24": "+CRSM: 144,0,43058441aa890affffffffffffffffffffffffffffffffff",
	"176,28472,0,0,15": "+CRSM: 144,0,ff30ffff3c003c03000c0000f03f00",
	"176,28438,0,0,2":  "+CRSM: 144,0,0233",
	"192,28436,0,0,15": "+CRSM: 144,0,000000146f1404001aa0aa01020000",
	"192,28615,0,0,15": "+CRSM: 144,0,000000406fc7040011a0aa01020120",
	"176,28436,0,0,20": "+CRSM: 144,0,416e64726f6964ffffffffffffffffffffffffff",
	"178,28615,1,4,32": "+CRSM: 144,0,566f6963656d61696cffffffffffffffffff07915155125740f9ffffffffffff",
}

func parseAssignment(s string) (ret string, waserror bool) {
	// Default response
	ret = ""
	waserror = false

	// Chop off the "AT+", if it is there, or remove an initial "+"
	s = strings.Replace(s, "AT+", "", 1)
	if s[0] == '+' {
		s = s[1:]
	}

	subs := strings.Split(s, "=")
	args := strings.Split(subs[1], ",")

	switch subs[0] {
	case "CGREG":
		fmt.Printf("setting CGREG to %s\n", args[0])
		modemstatus.cgreg, _ = strconv.Atoi(args[0])
		ret = "\r\n+CGREG: " + args[0] + "\r\n"
	case "CREG":
		fmt.Printf("setting CREG to %s\n", args[0])
		modemstatus.creg, _ = strconv.Atoi(args[0])
	case "CPIN":
		fmt.Print(s)
		modemstatus.pin, _ = strconv.Atoi(args[0])
		fmt.Printf("storing %d as the new pin, expected pin = %d\n", modemstatus.pin, expectedpin)
	case "COPS":
		if mode, _ := strconv.Atoi(args[0]); mode == 3 {
			modemstatus.regformat, _ = strconv.Atoi(args[1])
		} else if mode == 0 || mode == 1 {
			modemstatus.regstatus = mode
		}
	case "CRSM":
		//ret = "\r\n+CRSM: 0,0\r\n"
		if CRSMtab[subs[1]] == "" {
			ret = "\r\n+CME ERROR: BAD COMMAND\r\n"
			waserror = true
		} else {
			ret = "\r\n" + CRSMtab[subs[1]] + "\r\n"
		}
	case "CSMS":
		ret = "\r\n+CSMS: 1,1,1\r\n"
	case "CMGS":
		moutput.WriteString("\r\n> ")
		moutput.Flush()
		msg, _ := minput.ReadString('') // ctrl-z
		fmt.Printf("read message %v\n", msg)
		msg = msg[:len(msg)-1]
		pdu, err := pdu.PduUnpack(msg)
		if err == nil {
			fmt.Printf("Parsed SMS: %#v\n", pdu)
			ret = "\r\n+CMGS: " + strconv.Itoa(int(modemstatus.mr)) + "\r\n"
			modemstatus.mr++
		} else {
			ret = "ERROR"
		}
	}

	return
}

func otherMessage(s string) (ret string, waserror bool) {
	ret = ""
	waserror = false

	// Chop off the "AT+", if it is there, or remove an initial "+"
	s = strings.Replace(s, "AT+", "", 1)
	if s[0] == '+' {
		s = s[1:]
	}

	switch s {
	case "CGSN":
		// Get the IMEI
		ret = "\r\n135790248939\r\n"
	case "CIMI":
		// request the IMSI
		// give back the MCC + MNC + IMSI
		ret = "\r\n001011234567890\r\n"
	case "CSQ":
		// request signal strength
		ret = "\r\n+CSQ: 20,0\r\n" 
	}
	return
}

func main() {
	flag.Usage = usage
	flag.Parse()

	fi, err := os.Stat(*socketLoc)
	if err == nil && (fi.Mode() & os.ModeSocket) != 0 {
		os.Remove(*socketLoc)	// We'll remove any old sockets
	} else if err == nil {
		fmt.Fprintf(os.Stderr, "Specified file is not a unix socket, not removing\n")
		os.Exit(1)
	}
		

	l, err := net.Listen("unix", *socketLoc)
	if err != nil {
		fmt.Printf("error!")
		return
	}

	c, err := l.Accept()
	if err != nil {
		fmt.Printf("error in accept")
		return
	}

	r := bufio.NewReader(c)
	w := bufio.NewWriter(c)
	moutput = w
	minput = r

	modemstatus.regformat = 2
	modemstatus.regstatus = 0

	go cgregmsg()
	go cregmsg()

	var tmp string
	for {
		founderr := false
		line, err := r.ReadString('\r')
		if err != nil {
			fmt.Printf("failed to read from socket, exiting\n")
			return
		}
		line = line[:len(line)-1]
		line = strings.Replace(line, "\r\n", "", 1)
		if len(line) <= 1 {
			continue
		}
		fmt.Print("< " + line + "\n")
		subs := strings.Split(line, ";")
		out := ""
		waserr := false
		for _, s := range subs {
			if m, _ := regexp.MatchString(querypattern, s); m {
				tmp, waserr = parseQuery(s)
				out = out + tmp
			} else if m, _ := regexp.MatchString(assignpattern, s); m {
				tmp, waserr = parseAssignment(s)
				out = out + tmp
			} else {
				tmp, waserr = otherMessage(s)
				out = out + tmp
			}
			if waserr == true {
				founderr = true
			}
		}
		if founderr == false {
			out = out + OK
		}
		fmt.Printf("> %s\n", out)
		w.WriteString(out)
		w.Flush()
	}
}
